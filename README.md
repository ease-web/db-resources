This project is for storing CB_resources of ease-web. Because the files are large and will affect the performance of git pull/clone on Openshift/Jenkins, we use Git lfs to store them. Refer to [this article](https://about.gitlab.com/blog/2017/01/30/getting-started-with-git-lfs-tutorial/)

Before start, you need follow [the document](https://git-lfs.github.com/) to install Git LFS on your local first.

这个项目是用于保存ease web项目里面的CB_resources. 因为CB_resources里面的文件都挺大的，并且会影响到Openshift/Jenkins build的性能，于是我们使用Git LFS来储存这些大文件。请参考[这篇文章](https://about.gitlab.com/blog/2017/01/30/getting-started-with-git-lfs-tutorial/)

在开始之前，你需要根据[这个文档](https://git-lfs.github.com/)来在你本地安装Git LFS